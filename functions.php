<?php
    // ----------- setup -----------------*
    // -------------- soporte de thumbnails -----*
    add_theme_support( 'post-thumbnails' ); 
    
    // -------------- remove emoji --------*
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

    /* ----------------------- templates -------------------------------------- */

    // ----------- Estilos y scripts -----*
    function theme_styls(){
        wp_enqueue_style( 'normalize', get_template_directory_uri() . '/estilos/normalize.css', array(), '1.1' );
        wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/estilos/font-awesome.css', array(), '1.1' );
        wp_enqueue_style( 'style', get_template_directory_uri() . '/estilos/style.css', array(), '1.1' );
        wp_enqueue_style( 'responsive', get_template_directory_uri() . '/estilos/medias.css', array(), '1.1' );
        
    	wp_enqueue_script( 'jquery2', get_template_directory_uri() . '/scripts/jquery-2.1.4.min.js', array(), '20141010', true );
        wp_enqueue_script( 'MyScript', get_template_directory_uri() . '/scripts/globalScript.js', array(), '20141010', true );

    }
    add_action( 'wp_enqueue_scripts', 'theme_styls' );

    // --------------- Menu ---------*
    function theme_menu_pr() {
        register_nav_menu('main-menu',__( 'Menu Principal' ));
    }
    add_action( 'init', 'theme_menu_pr' );
	
    function theme_widget() {
        register_sidebar( array(
        'name' => __( 'Sidebar principal', 'main-sidebar' ),
        'id'            => 'main-sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="widgettitle">',
        'after_title'   => '</div>',
        ) );
        register_sidebar( array(
            'name' => 'top slider',
            'id' => 'top-slider',
            'before_widget' => '<div class="top-slider">',
            'after_widget' => '</div>'
        ) );
        register_sidebar( array(
            'name' => 'front page home',
            'id' => 'front-sider',
            'before_widget' => '<div class="top-slider">',
            'after_widget' => '</div>'
        ) );
    }
    
    add_action( 'widgets_init', 'theme_widget' );

    // -------------- Acortador de Post ----------*
    function theme_excerpt( $length ) {
	   return 200;
    }
    add_filter( 'excerpt_length', 'theme_excerpt', 999 );
    
    function theme_excerpt_more( $more ){
        $title = get_permalink( get_the_ID() );
        return '<br> <a href="'.$title.'">Seguir leyendo . . . </a>';
    }
    add_filter('excerpt_more', 'theme_excerpt_more');
    
    // -------------- Funcionalidades extras ------*
    function theme_links($red = ""){
        $content = wp_get_shortlink();
        $content = urlencode($content);
        //$content = str_replace(" ", "%", $content);
        
        if($red == "Twitter"){
            $cont = urlencode(get_the_title());
            $content = $content." ". $cont; // no olvidar poner formulario para hastags
        }
        print $content;

    }
    function theme_class_ct($extra = ""){
        if( is_page() ):
            echo 'class="content page '.$extra.'"';
        elseif( is_single() ):
            echo 'class="content single '.$extra.'"';
        elseif(is_home()):
            echo 'class="content blog '.$extra.'"';
        else:
            echo 'class="content posts '.$extra.'"';
        endif;
    }