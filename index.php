<?php 
    get_header();
    ?>
    <div id="slider">
        <div id="wrap">
            <?php if ( is_active_sidebar( 'top-slider' ) ) : ?>
                    <?php dynamic_sidebar( 'top-slider' ); ?>
            <?php endif; ?>

        </div>
    </div>
    <main id="main-content" <?php theme_class_ct() ?>>
        <div id="div-c">
            <div id="wrap">
                <?php 
                    get_template_part('content');
                ?>
            </div>
        </div>
    </main>
    <?php
    get_footer();
?>