<?php
/*
    Template Name: Page home
    Description: Page home
*/

get_header();
?>
<id id="slider">
	<?php

		if ( is_active_sidebar( 'front-sider' ) ) :
				dynamic_sidebar( 'front-sider' );
		endif;
	?>
</div>
<main>
    <?php
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
				the_content();
			endwhile;
		endif;
    ?>
</main>
<?php
get_footer();